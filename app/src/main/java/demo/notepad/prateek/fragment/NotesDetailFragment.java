package demo.notepad.prateek.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import demo.notepad.prateek.R;
import demo.notepad.prateek.database.NotesDataUtils;
import demo.notepad.prateek.model.NotesItem;

/**
 * Created by Prateek on 08-12-2016.
 */

public class NotesDetailFragment extends Fragment implements Toolbar.OnMenuItemClickListener,View.OnTouchListener {

    private NotesItem detail;
    private EditText mTitle;
    private EditText mDescription;
    private String mImageUri;
    private ImageView mAttachment;
    private final int GALLERY_REQUEST_CODE = 0x01;
    private final int PERMISSION_WRITE_STORAGE_GALLERY = 0x02;
    private final int PERMISSION_BITMAP = 0x03;
    private final ColorFilter grayFilter = new PorterDuffColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);

    public static NotesDetailFragment newInstance(NotesItem detail){

        NotesDetailFragment fragment = new NotesDetailFragment();
        fragment.detail = detail;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_notes,container,false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        mTitle = (EditText)getView().findViewById(R.id.title_edit);
        mDescription = (EditText)getView().findViewById(R.id.desc_edit);
        mAttachment = (ImageView)getView().findViewById(R.id.attachement);
        if(detail == null)
        toolbar.inflateMenu(R.menu.detail_menu_add);
        else{
            toolbar.inflateMenu(R.menu.detail_menu_view);
            mTitle.setText(detail.getTitle());
            mDescription.setText(detail.getDesc());
            mImageUri = detail.getMedia();

            mAttachment.setOnTouchListener(this);
            setImage();

        }
        toolbar.setOnMenuItemClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK){

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            this.mImageUri = cursor.getString(columnIndex);
          setImage();
        }
    }

    private void setImage(){


        if(TextUtils.isEmpty(mImageUri))
            return;

        if(!isPermissionValid(PERMISSION_BITMAP))
            return;
        String temp = getContext().getCacheDir()+"temp_"+ System.currentTimeMillis();
        decodeSampledBitmapFromFile(this.mImageUri,600,600,temp);
        mAttachment.setImageBitmap(BitmapFactory.decodeFile(temp));
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        switch (item.getItemId()){
            case R.id.action_save:
                saveNote();
                break;
            case R.id.action_gallery:
                pickImageFromGallery();
                break;
            case R.id.action_delete:{
                NotesDataUtils.deleteNote(getContext(),detail.getId());
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
                break;
        }
        return true;
    }




    private void saveNote(){

        String title = mTitle.getText().toString();

        if(TextUtils.isEmpty(title)){
            Toast.makeText(getContext(),"Please provide title",Toast.LENGTH_SHORT).show();
            return;
        }

        String desc = mDescription.getText().toString();

        if(TextUtils.isEmpty(desc)){
            Toast.makeText(getContext(),"Please provide description",Toast.LENGTH_SHORT).show();
            return;
        }

        NotesItem item = null;
        if(this.detail == null)
            item = new NotesItem();
        else
            item = this.detail;
        item.setTitle(title);
        item.setDesc(desc);
        if(!TextUtils.isEmpty(mImageUri))
            item.setMedia(mImageUri);

        NotesDataUtils.saveNote(getContext(),item);
        getActivity().getSupportFragmentManager().popBackStackImmediate();


    }

    private void decodeSampledBitmapFromFile(String pathOfInputImage,
                                               int dstWidth, int dstHeight,String pathOfOutputImage) {

        try
        {
            int inWidth = 0;
            int inHeight = 0;

            InputStream in = new FileInputStream(pathOfInputImage);

            // decode image size (decode metadata only, not the whole image)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            in.close();
            in = null;

            // save width and height
            inWidth = options.outWidth;
            inHeight = options.outHeight;

            // decode full image pre-resized
            in = new FileInputStream(pathOfInputImage);
            options = new BitmapFactory.Options();
            // calc rought re-size (this is no exact resize)
            options.inSampleSize = Math.max(inWidth/dstWidth, inHeight/dstHeight);
            // decode full image
            Bitmap roughBitmap = BitmapFactory.decodeStream(in, null, options);

            // calc exact destination size
            Matrix m = new Matrix();
            RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
            RectF outRect = new RectF(0, 0, dstWidth, dstHeight);
            m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
            float[] values = new float[9];
            m.getValues(values);

            // resize bitmap
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);

            // save image
            try
            {
                FileOutputStream out = new FileOutputStream(pathOfOutputImage);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            }
            catch (Exception e)
            {
                Log.e("Image", e.getMessage(), e);
            }
        }
        catch (IOException e)
        {
            Log.e("Image", e.getMessage(), e);
        }
    }



    private void pickImageFromGallery(){

       if(!isPermissionValid(PERMISSION_WRITE_STORAGE_GALLERY))
           return;

        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, GALLERY_REQUEST_CODE);
    }

    private boolean isPermissionValid(int request){
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

                requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        request);


            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_WRITE_STORAGE_GALLERY: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                  pickImageFromGallery();

                } else {
                        Toast.makeText(getContext(),
                                "We don't have permission to access gallery.",Toast.LENGTH_SHORT).show();
                }
                return;

            }
            case PERMISSION_BITMAP: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                  setImage();

                } else {
                    Toast.makeText(getContext(),
                            "We don't have permission to access gallery.",Toast.LENGTH_SHORT).show();
                }
                return;

            }

        }
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {


        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:{
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                mAttachment.setColorFilter(filter);
                return true;}
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                mAttachment.clearColorFilter();
                return true;


        }
        return false;
    }
}
