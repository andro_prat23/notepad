package demo.notepad.prateek.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import demo.notepad.prateek.R;
import demo.notepad.prateek.adapter.ListAdapter;
import demo.notepad.prateek.callbacks.ItemClickListener;
import demo.notepad.prateek.database.NotesDataUtils;
import demo.notepad.prateek.model.NotesItem;

/**
 * Created by Prateek on 08-12-2016.
 */

public class NotesListFragment extends Fragment implements ItemClickListener {

    private RecyclerView mNotesList;
    public static NotesListFragment newInstance(){

        NotesListFragment fragment = new NotesListFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notes_list,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mNotesList  =(RecyclerView)getView().findViewById(R.id.notes_list);
        mNotesList.setLayoutManager(new LinearLayoutManager(getContext()));
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        toolbar.setTitle("NotePad");
        FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDetailScreen(null);
            }
        });

        initializeList();
    }

    private void initializeList(){

        List<NotesItem> list = NotesDataUtils.getNotes(this.getContext());
        ListAdapter mAdapter = new ListAdapter(getContext(),list,this);
        mNotesList.setAdapter(mAdapter);

    }

    private void showDetailScreen(NotesItem item){
        NotesDetailFragment fragment = NotesDetailFragment.newInstance(item);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container,fragment);
        transaction.addToBackStack("Detail");
        transaction.commit();
    }

    @Override
    public void showDetail(NotesItem item) {

        showDetailScreen(item);
    }
}
