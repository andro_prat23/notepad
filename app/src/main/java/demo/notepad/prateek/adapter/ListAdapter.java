package demo.notepad.prateek.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import demo.notepad.prateek.R;
import demo.notepad.prateek.callbacks.ItemClickListener;
import demo.notepad.prateek.model.NotesItem;

/**
 * Created by Prateek on 08-12-2016.
 */

public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<NotesItem> itemList;
    private ItemClickListener mCallback;
    public ListAdapter(Context context, List<NotesItem> list, ItemClickListener listener){
        this.mContext = context;
        this.itemList = list;
        this.mCallback = listener;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotesViewHolder(LayoutInflater.from(mContext).inflate(R.layout.notes_item,parent,false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder != null){
            ((NotesViewHolder)holder).bindData(itemList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    private class NotesViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView desc;
        private TextView createdDate;
        private View mRoot;
        public NotesViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title_item);
            desc = (TextView)itemView.findViewById(R.id.desc_item);
            createdDate = (TextView)itemView.findViewById(R.id.created_date);
            mRoot = itemView;

        }

        private void bindData(final NotesItem item){
            if(item != null){
                if(mCallback != null)
                    mRoot.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCallback.showDetail(item);
                        }
                    });
                title.setText(item.getTitle());
                desc.setText(item.getDesc());
                createdDate.setText(item.getCreatedDate());
            }
        }
    }
}
