package demo.notepad.prateek.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import demo.notepad.prateek.model.NotesItem;

/**
 * Created by Prateek on 08-12-2016.
 */

public class NotesDataUtils {


    public static List<NotesItem> getNotes(Context activity){

        if(activity == null )
                return null;

        List<NotesItem> result = new ArrayList<>();
        String[] PROJECTION = new String[] {
                Contract.Notes._ID,
                Contract.Notes.COLUMN_NAME_TITLE,
                Contract.Notes.COLUMN_NAME_NOTE_DESC,
                Contract.Notes.COLUMN_NAME_MEDIA,
                Contract.Notes.COLUMN_NAME_CREATE_DATE
        };
        Cursor cursor = activity.getContentResolver().query(Contract.Notes.CONTENT_URI,PROJECTION,
                null,null,Contract.Notes.DEFAULT_SORT_ORDER);

        if(cursor == null || cursor.getCount() <= 0 ){
            return result;
        }

        while (cursor.moveToNext()){

            NotesItem item = new NotesItem();
            item.setId(cursor.getInt(cursor.getColumnIndex(Contract.Notes._ID)));
            item.setTitle(cursor.getString(cursor.getColumnIndex(Contract.Notes.COLUMN_NAME_TITLE)));
            item.setDesc(cursor.getString(cursor.getColumnIndex(Contract.Notes.COLUMN_NAME_NOTE_DESC)));
            item.setMedia(cursor.getString(cursor.getColumnIndex(Contract.Notes.COLUMN_NAME_MEDIA)));

            long time = cursor.getLong(cursor.getColumnIndex(Contract.Notes.COLUMN_NAME_CREATE_DATE));
            Date date = new Date();
            date.setTime(time);
            SimpleDateFormat pattern = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
            item.setCreatedDate( pattern.format(date));

            result.add(item);
        }

        return result;
    }


    public static void saveNote(Context context,NotesItem item){

        ContentValues values = new ContentValues();
        values.put(Contract.Notes.COLUMN_NAME_TITLE,item.getTitle());
        values.put(Contract.Notes.COLUMN_NAME_NOTE_DESC,item.getDesc());
        values.put(Contract.Notes.COLUMN_NAME_MEDIA,item.getMedia());

        if(context == null)
               return;

        ContentResolver resolver = context.getContentResolver();
        if(item.getId() < 0){
            resolver.insert(Contract.Notes.CONTENT_URI,values);
            return;
        }else{
            Uri temp = Uri.withAppendedPath(Contract.Notes.CONTENT_ID_URI_BASE,item.getId()+"")  ;
            resolver.update(temp,values,null,null);
        }
    }

    public static int deleteNote(Context context,int id){

        return context.getContentResolver().delete(Uri.withAppendedPath(Contract.Notes.CONTENT_ID_URI_BASE,id+""),null,null);
    }
}
