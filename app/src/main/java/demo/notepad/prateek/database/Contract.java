package demo.notepad.prateek.database;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Prateek on 08-12-2016.
 */

public final class Contract {
    public static final String AUTHORITY = "demo.notepad.prateek.contract";

    private Contract() {
    }

    public static final class Notes implements BaseColumns {
        private Notes() {}

        public static final String TABLE_NAME = "notes";

        private static final String SCHEME = "content://";

        private static final String PATH_NOTES = "/notes";

        private static final String PATH_NOTE_ID = "/notes/";

        public static final int NOTE_ID_PATH_POSITION = 1;


        public static final Uri CONTENT_URI =  Uri.parse(SCHEME + AUTHORITY + PATH_NOTES);

        public static final Uri CONTENT_ID_URI_BASE
                = Uri.parse(SCHEME + AUTHORITY + PATH_NOTE_ID);

        public static final Uri CONTENT_ID_URI_PATTERN
                = Uri.parse(SCHEME + AUTHORITY + PATH_NOTE_ID + "/#");

        public static final String DEFAULT_SORT_ORDER = "created DESC";

        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.note";

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.note";

        public static final String COLUMN_NAME_TITLE = "title";

        public static final String COLUMN_NAME_NOTE_DESC = "note";

        public static final String COLUMN_NAME_MEDIA = "photo";

        public static final String COLUMN_NAME_CREATE_DATE = "created";

    }
}
