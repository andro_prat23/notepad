package demo.notepad.prateek.database;


import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.HashMap;
/**
 * Created by Prateek on 08-12-2016.
 */
public class Provider extends ContentProvider {

    private static HashMap<String, String> sNotesProjectionMap;
    private static final int NOTES = 1;
    private static final int NOTE_ID = 2;
    private static final UriMatcher sUriMatcher;
    private DatabaseHelper mOpenHelper;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(Contract.AUTHORITY, "notes", NOTES);
        sUriMatcher.addURI(Contract.AUTHORITY, "notes/#", NOTE_ID);

        sNotesProjectionMap = new HashMap<String, String>();
        sNotesProjectionMap.put(Contract.Notes._ID, Contract.Notes._ID);
        sNotesProjectionMap.put(Contract.Notes.COLUMN_NAME_TITLE, Contract.Notes.COLUMN_NAME_TITLE);
        sNotesProjectionMap.put(Contract.Notes.COLUMN_NAME_NOTE_DESC, Contract.Notes.COLUMN_NAME_NOTE_DESC);
        sNotesProjectionMap.put(Contract.Notes.COLUMN_NAME_MEDIA,Contract.Notes.COLUMN_NAME_MEDIA);
        sNotesProjectionMap.put(Contract.Notes.COLUMN_NAME_CREATE_DATE,
                Contract.Notes.COLUMN_NAME_CREATE_DATE);


    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(Contract.Notes.TABLE_NAME);
        switch (sUriMatcher.match(uri)) {
            case NOTES:
                qb.setProjectionMap(sNotesProjectionMap);
                break;
            case NOTE_ID:
                qb.setProjectionMap(sNotesProjectionMap);
                qb.appendWhere(
                        Contract.Notes._ID +
                                "=" +
                                uri.getPathSegments().get(Contract.Notes.NOTE_ID_PATH_POSITION));
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = Contract.Notes.DEFAULT_SORT_ORDER;
        } else {
            orderBy = sortOrder;
        }
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(
                db,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                orderBy
        );
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }
    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case NOTES:

                return Contract.Notes.CONTENT_TYPE;
            case NOTE_ID:
                return Contract.Notes.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        if (sUriMatcher.match(uri) != NOTES) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }
        Long now = Long.valueOf(System.currentTimeMillis());
        if (values.containsKey(Contract.Notes.COLUMN_NAME_CREATE_DATE) == false) {
            values.put(Contract.Notes.COLUMN_NAME_CREATE_DATE, now);
        }

        if (values.containsKey(Contract.Notes.COLUMN_NAME_TITLE) == false) {
            Resources r = Resources.getSystem();
            values.put(Contract.Notes.COLUMN_NAME_TITLE, r.getString(android.R.string.untitled));
        }
        if (values.containsKey(Contract.Notes.COLUMN_NAME_NOTE_DESC) == false) {
            values.put(Contract.Notes.COLUMN_NAME_NOTE_DESC, "");
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = db.insert(
                Contract.Notes.TABLE_NAME,
                Contract.Notes.COLUMN_NAME_NOTE_DESC,
                values
        );
        if (rowId > 0) {
            Uri noteUri = ContentUris.withAppendedId(Contract.Notes.CONTENT_ID_URI_BASE, rowId);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        String finalWhere;
        int count;
        switch (sUriMatcher.match(uri)) {
            case NOTES:
                count = db.delete(
                        Contract.Notes.TABLE_NAME,
                        where,
                        whereArgs
                );
                break;
            case NOTE_ID:
                finalWhere =
                        Contract.Notes._ID +
                                " = " +
                                uri.getPathSegments().
                                        get(Contract.Notes.NOTE_ID_PATH_POSITION)
                ;
                if (where != null) {
                    finalWhere = finalWhere + " AND " + where;
                }
                count = db.delete(
                        Contract.Notes.TABLE_NAME,
                        finalWhere,
                        whereArgs
                );
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        String finalWhere;
        switch (sUriMatcher.match(uri)) {
            case NOTES:
                count = db.update(
                        Contract.Notes.TABLE_NAME,
                        values,
                        where,
                        whereArgs
                );
                break;
            case NOTE_ID:
                String noteId = uri.getPathSegments().get(Contract.Notes.NOTE_ID_PATH_POSITION);
                finalWhere =
                        Contract.Notes._ID +
                                " = " +
                                uri.getPathSegments().
                                        get(Contract.Notes.NOTE_ID_PATH_POSITION)
                ;
                if (where !=null) {
                    finalWhere = finalWhere + " AND " + where;
                }
                count = db.update(
                        Contract.Notes.TABLE_NAME,
                        values,
                        finalWhere,
                        whereArgs
                );
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

}
