package demo.notepad.prateek.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Prateek on 08-12-2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "Helper";
    private static final String DATABASE_NAME = "notepad.db";
    private static final int DATABASE_VERSION = 3;
    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Contract.Notes.TABLE_NAME + " ("
                + Contract.Notes._ID + " INTEGER PRIMARY KEY,"
                + Contract.Notes.COLUMN_NAME_TITLE + " TEXT,"
                + Contract.Notes.COLUMN_NAME_NOTE_DESC + " TEXT,"
                + Contract.Notes.COLUMN_NAME_MEDIA+" TEXT,"
                + Contract.Notes.COLUMN_NAME_CREATE_DATE + " INTEGER"
                + ");");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS notes");
        onCreate(db);
    }
}
