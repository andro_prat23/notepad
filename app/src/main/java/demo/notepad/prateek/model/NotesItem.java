package demo.notepad.prateek.model;

import android.net.Uri;

/**
 * Created by Prateek on 08-12-2016.
 */

public class NotesItem {

    private String title;
    private String desc;
    private String media;
    private String createdDate;
    private int id = Integer.MIN_VALUE;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
