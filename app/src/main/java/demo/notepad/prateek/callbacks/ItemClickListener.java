package demo.notepad.prateek.callbacks;

import demo.notepad.prateek.model.NotesItem;

/**
 * Created by Prateek on 08-12-2016.
 */

public interface ItemClickListener {

    public void showDetail(NotesItem item);
}
